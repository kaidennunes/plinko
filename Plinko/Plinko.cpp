// Plink.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <iostream>
#include <iomanip>
#include <time.h>

using namespace std;

/*
1. Drop one chip into one slot
2. Drop multiple chips into one slot
3. Drop multiple chips (the same number) into every slot
4. Quit
1

Which slot do you want to put your chip in?
8

8, 7.5, 7, 6.5, 6, 5.5, 5, 4.5, 5, 5.5, 6, 6.5, 6
You won $1000

1. Drop one chip into one slot
2. Drop multiple chips into one slot
3. Drop multiple chips (the same number) into every slot
4. Quit
1

Which slot do you want to put your chip in?
-1

Chip positions only range from 0 to 8

1. Drop one chip into one slot
2. Drop multiple chips into one slot
3. Drop multiple chips (the same number) into every slot
4. Quit
4

Goodbye





1. Drop one chip into one slot
2. Drop multiple chips into one slot
3. Drop multiple chips (the same number) into every slot
4. Quit
2

How many chips do you want to enter?
8

Which slot do you want to put your chip in?
0

You won a total of $2800
For each chip, you won an average of $350

1. Drop one chip into one slot
2. Drop multiple chips into one slot
3. Drop multiple chips (the same number) into every slot
4. Quit
4

Goodbye






1. Drop one chip into one slot
2. Drop multiple chips into one slots
3. Drop multiple chips (the same number) into every slot
4. Quit
2

How many chips do you want to enter?
-3

You cannot have a negative number of chips

1. Drop one chip into one slot
2. Drop multiple chips into one slot
3. Drop multiple chips (the same number) into every slot
4. Quit
2

How many chips do you want to enter?
16

Which slot do you want to put your chip in?
9

Chip positions only range from 0 to 8

1. Drop one chip into one slot
2. Drop multiple chips into one slot
3. Drop multiple chips (the same number) into every slot
4. Quit
2

How many chips do you want to enter?
9

Which slot do you want to put your chip in?
0

You won a total of $3400
For each chip, you won an average of $377.777

1. Drop one chip into one slot
2. Drop multiple chips into one slot
3. Drop multiple chips (the same number) into every slot
4. Quit
3

How many chips do you want to enter?
10

For slot 0, you won a total of $2000
For each chip in slot 0, you won an average of $200

For slot 1, you won a total of $24000
For each chip in slot 1, you won an average of $240

For slot 2, you won a total of $14200
For each chip in slot 2, you won an average of $1420

For slot 3, you won a total of $12500
For each chip in slot 3, you won an average of $1250

For slot 4, you won a total of $5000
For each chip in slot 4, you won an average of $500

For slot 5, you won a total of $13600
For each chip in slot 5, you won an average of $1360

For slot 6, you won a total of $13100
For each chip in slot 6, you won an average of $1310

For slot 7, you won a total of $4600
For each chip in slot 7, you won an average of $460

For slot 8, you won a total of $15800
For each chip in slot 8, you won an average of $1580

1. Drop one chip into one slot
2. Drop multiple chips into one slot
3. Drop multiple chips (the same number) into every slot
4. Quit
4

Goodbye
*/

//  Returns the prize money value
int getPrizeMoney(double chipPosition)
{
	// The prize money for each slot
	const int slotZero = 100;
	const int slotOne = 500;
	const int slotTwo = 1000;
	const int slotThree = 0;
	const int slotFour = 10000;
	const int slotFive = 0;
	const int slotSix = 1000;
	const int slotSeven = 500;
	const int slotEight = 100;

	if (chipPosition == 0)
	{
		return slotZero;
	}
	else if (chipPosition == 1)
	{
		return slotOne;
	}
	else if (chipPosition == 2)
	{
		return slotTwo;
	}
	else if (chipPosition == 3)
	{
		return slotThree;
	}
	else if (chipPosition == 4)
	{
		return slotFour;
	}
	else if (chipPosition == 5)
	{
		return slotFive;
	}
	else if (chipPosition == 6)
	{
		return slotSix;
	}
	else if (chipPosition == 7)
	{
		return slotSeven;
	}
	else
	{
		return slotEight;
	}
}

// @ printChip true, print position of each chip
// This prints out the position of each chip if the boolean value printChip is true
int dropChip(double chipPosition, int height, int width, bool printChip)
{
	if (printChip)
	{
		// Prints position the chip was put in
		cout << endl << chipPosition << ", ";
	}
	int currentHeight = height;
	while (currentHeight > 1)
	{
		// Gives me a number between 0 and 1
		// 0 makes the position go smaller
		// 1 makes the position go larger
		int randomNumber = rand() % 2;

		if (randomNumber == 0)
		{
			// Ensures the chip doesn't go out of the board
			if (chipPosition == width)
			{
				chipPosition -= .5;
			}
			else
			{
				chipPosition += .5;
			}
		}
		else
		{
			if (chipPosition == 0)
			{
				chipPosition += .5;
			}
			else
			{
				chipPosition -= .5;
			}
		}
		currentHeight--;
		// Prints instead of the second if
		// Only when the chip is at the bottom of the board
		if (currentHeight == 1 && printChip)
		{
			// Makes sure there is no comma at the final print position
			// Creates a newline to print the prize money on
			cout << chipPosition << endl;
		}
		else if (printChip)
		{
			cout << chipPosition << ", ";
		}
	}
	return getPrizeMoney(chipPosition);
}

int dropMultipleChips(double chipPosition, int numbOfChips, int height, int width)
{
	// False because we do not want to print the chip locations
	bool printChip = false;
	int totalPrizeMoney = 0;
	for (int i = 0; i < numbOfChips; i++)
	{
		totalPrizeMoney += dropChip(chipPosition, height, width, printChip);
	}
	return totalPrizeMoney;
}

int main()
{
	// Defines how big the board is
	// This is more like index
	// Eight slots to put chip in
	// Thirteen slots vertically for chip to be in while falling
	const int width = 8;
	const int height = 13;

	while (true)
	{
		// Main menu
		int menuUserInput;
		cout << "1. Drop one chip into one slot" << endl;
		cout << "2. Drop multiple chips into one slot" << endl;
		cout << "3. Drop multiple chips (the same number) into every slot" << endl;
		cout << "4. Quit" << endl;

		// As long as the user inputs invalid option
		// Continue to clear cin
		// And ask again
		while (!(cin >> menuUserInput))
		{
			cout << "\nPlease enter an integer of the option you want" << endl;
			cin.clear();
			cin.ignore(10000, '\n');
		}

		// Random seed
		srand(time(0));

		// Break out of loop and quit
		if (menuUserInput == 4)
		{
			cout << "\nGoodbye" << endl;
			break;
		}
		else if (menuUserInput == 1)
		{
			// True because we do want to print the chip positions
			bool printChip = true;
			double chipPosition;
			cout << "\nWhich slot do you want to put your chip in?" << endl;
			cin >> chipPosition;

			// If the slot they want to enter chip does not exist
			// Return user to menu
			if (chipPosition< 0 || chipPosition>width)
			{
				// Print error message
				// and return the user to main menu
				cout << "\nChip positions only range from 0 to " << width << endl << endl;
			}
			else
			{
				// True means you want to print out the position of the chip
				int prizeMoney = dropChip(chipPosition, height, width, printChip);
				cout << "You won $" << prizeMoney << endl << endl;
			}
		}
		else if (menuUserInput == 2)
		{
			int numberOfChips;
			cout << "\nHow many chips do you want to enter?" << endl;
			cin >> numberOfChips;

			// If number of chips is non positive including zero
			// Return user to menu
			if (numberOfChips <= 0)
			{
				// Print error message
				// and return the user to main menu
				cout << "\nYou cannot have a negative number of chips" << endl << endl;
			}
			else
			{
				double chipPosition;
				cout << "\nWhich slot do you want to put your chip in?" << endl;
				cin >> chipPosition;
				// If the slot they want to enter chip does not exist
				// Return user to menu
				if (chipPosition< 0 || chipPosition>width)
				{
					// Print error message
					// and return the user to main menu
					cout << "\nChip positions only range from 0 to " << width << endl << endl;
				}
				else
				{
					int	prizeMoneyTotal = dropMultipleChips(chipPosition, numberOfChips, height, width);
					cout << "\nYou won a total of $" << prizeMoneyTotal << endl;
					cout << "For each chip, you won an average of $" << double(prizeMoneyTotal) / numberOfChips << endl << endl;
				}
			}
		}
		else if (menuUserInput == 3)
		{
			int numberOfChips;
			cout << "\nHow many chips do you want to enter?" << endl;
			cin >> numberOfChips;

			// If number of chips is non positive including zero
			// Return user to menu
			if (numberOfChips <= 0)
			{
				// Print error message
				// and return the user to main menu
				cout << "\nYou cannot have a negative number of chips" << endl << endl;
			}
			else
			{
				// For each slot in the board
				// Drop the number of chips in the slot
				for (int chipPosition = 0; chipPosition <= width; chipPosition++)
				{
					int prizeMoneyTotal = dropMultipleChips(chipPosition, numberOfChips, height, width);
					cout << "\nFor slot " << chipPosition <<", you won a total of $" << prizeMoneyTotal << endl;
					cout << "For each chip in slot " << chipPosition <<", you won an average of $" << double(prizeMoneyTotal) / numberOfChips << endl << endl;
				}
			}
		}
		else
		{
			cout << "\nThis is not a menu option" << endl << endl;
		}
	}
	system("pause");
	return 0;
}